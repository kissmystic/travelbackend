"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DB = void 0;
const client_1 = require("@prisma/client");
function DB(target, _propertyKey) {
    return async function (props) {
        const { response, request, token } = props;
        const prisma = new client_1.PrismaClient();
        const result = await target(Object.assign(Object.assign({}, props), { prisma }));
        await prisma.$disconnect();
        return result;
    };
}
exports.DB = DB;
