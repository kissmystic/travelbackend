"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Login = void 0;
const User_1 = __importDefault(require("../DBProvider/User"));
function Login(target, _propertyKey) {
    return async function (props) {
        var _a, _b, _c, _d;
        const { response, request } = props;
        console.log(request === null || request === void 0 ? void 0 : request.headers);
        console.log(request === null || request === void 0 ? void 0 : request.body);
        if (!((_a = request.body) === null || _a === void 0 ? void 0 : _a.email) || !((_b = request.body) === null || _b === void 0 ? void 0 : _b.password)) {
            return response.json({
                error: 'Authentication error 1',
            });
        }
        const token = await User_1.default.login({
            email: (_c = request.body) === null || _c === void 0 ? void 0 : _c.email,
            password: (_d = request.body) === null || _d === void 0 ? void 0 : _d.password,
        });
        if (!token) {
            return response.json({
                error: 'Authentication error 2',
            });
        }
        const data = await target({ token });
        response.json(data);
    };
}
exports.Login = Login;
