"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Registration = void 0;
const User_1 = __importDefault(require("../DBProvider/User"));
function Registration(target, _propertyKey) {
    return async function (props) {
        var _a, _b, _c, _d;
        const { response, request } = props;
        console.log(request === null || request === void 0 ? void 0 : request.headers);
        console.log(request === null || request === void 0 ? void 0 : request.body);
        if (!((_a = request.body) === null || _a === void 0 ? void 0 : _a.email) || !((_b = request.body) === null || _b === void 0 ? void 0 : _b.password)) {
            return response.json({
                error: 'Regsitration error',
            });
        }
        const userData = await User_1.default.registration({
            email: (_c = request.body) === null || _c === void 0 ? void 0 : _c.email,
            password: (_d = request.body) === null || _d === void 0 ? void 0 : _d.password,
        });
        if (!(userData === null || userData === void 0 ? void 0 : userData.token)) {
            return response.json({
                error: 'Authentication error',
            });
        }
        const data = await target({ token: userData === null || userData === void 0 ? void 0 : userData.token });
        response.json(data);
    };
}
exports.Registration = Registration;
