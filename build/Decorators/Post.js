"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Post = void 0;
const User_1 = __importDefault(require("../DBProvider/User"));
function Post(target, _propertyKey) {
    return async function (props) {
        var _a;
        const { response, request } = props;
        const token = ((_a = request === null || request === void 0 ? void 0 : request.body) === null || _a === void 0 ? void 0 : _a.token) || (request === null || request === void 0 ? void 0 : request.headers['token']);
        const userData = await User_1.default.getByToken({ token });
        const isAuthenticated = !!(userData === null || userData === void 0 ? void 0 : userData.token);
        if (!isAuthenticated) {
            return response.json({
                error: 'Authentication error',
            });
        }
        const data = await target(Object.assign(Object.assign({}, props), { token: userData.token, userData, body: Object.assign({}, request.body) }));
        response.json(data);
    };
}
exports.Post = Post;
