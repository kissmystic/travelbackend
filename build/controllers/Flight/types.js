"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FareClassTypes = void 0;
var FareClassTypes;
(function (FareClassTypes) {
    FareClassTypes["economy"] = "economy";
    FareClassTypes["premiumEconomy"] = "premium economy";
    FareClassTypes["business"] = "business";
    FareClassTypes["first"] = "first";
})(FareClassTypes || (exports.FareClassTypes = FareClassTypes = {}));
