"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const body_parser_1 = __importDefault(require("body-parser"));
const cors_1 = __importDefault(require("cors"));
class Router {
    constructor(controllers, params) {
        this.list = [];
        this.controllers = controllers;
        this.params = params;
        this.buildList();
        this.create();
    }
    buildList() {
        this.controllers.forEach((controller) => {
            Object.getOwnPropertyNames(controller).forEach((route) => {
                if (route !== 'length' && route !== 'name' && route !== 'prototype') {
                    this.list.push({
                        name: `/${controller.name.toLowerCase()}/${route}`,
                        method: route,
                        controller: controller,
                    });
                }
            });
        });
    }
    create() {
        const app = (0, express_1.default)();
        app.use(body_parser_1.default.urlencoded({
            extended: false,
        }));
        app.use(body_parser_1.default.json());
        app.use((0, cors_1.default)());
        for (let i = 0; i <= this.list.length - 1; i++) {
            const route = this.list[i];
            app.post(route.name, (request, response, next) => route.controller[route.method](Object.assign({ request,
                response,
                next }, this.params)));
        }
        app.listen('8002', function () {
            console.log('server started');
        });
    }
}
exports.default = Router;
