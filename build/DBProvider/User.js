"use strict";
var __esDecorate = (this && this.__esDecorate) || function (ctor, descriptorIn, decorators, contextIn, initializers, extraInitializers) {
    function accept(f) { if (f !== void 0 && typeof f !== "function") throw new TypeError("Function expected"); return f; }
    var kind = contextIn.kind, key = kind === "getter" ? "get" : kind === "setter" ? "set" : "value";
    var target = !descriptorIn && ctor ? contextIn["static"] ? ctor : ctor.prototype : null;
    var descriptor = descriptorIn || (target ? Object.getOwnPropertyDescriptor(target, contextIn.name) : {});
    var _, done = false;
    for (var i = decorators.length - 1; i >= 0; i--) {
        var context = {};
        for (var p in contextIn) context[p] = p === "access" ? {} : contextIn[p];
        for (var p in contextIn.access) context.access[p] = contextIn.access[p];
        context.addInitializer = function (f) { if (done) throw new TypeError("Cannot add initializers after decoration has completed"); extraInitializers.push(accept(f || null)); };
        var result = (0, decorators[i])(kind === "accessor" ? { get: descriptor.get, set: descriptor.set } : descriptor[key], context);
        if (kind === "accessor") {
            if (result === void 0) continue;
            if (result === null || typeof result !== "object") throw new TypeError("Object expected");
            if (_ = accept(result.get)) descriptor.get = _;
            if (_ = accept(result.set)) descriptor.set = _;
            if (_ = accept(result.init)) initializers.unshift(_);
        }
        else if (_ = accept(result)) {
            if (kind === "field") initializers.unshift(_);
            else descriptor[key] = _;
        }
    }
    if (target) Object.defineProperty(target, contextIn.name, descriptor);
    done = true;
};
var __runInitializers = (this && this.__runInitializers) || function (thisArg, initializers, value) {
    var useValue = arguments.length > 2;
    for (var i = 0; i < initializers.length; i++) {
        value = useValue ? initializers[i].call(thisArg, value) : initializers[i].call(thisArg);
    }
    return useValue ? value : void 0;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const md5_1 = __importDefault(require("md5"));
const DB_1 = require("../Decorators/DB");
let User = (() => {
    var _a;
    let _staticExtraInitializers = [];
    let _static_getByToken_decorators;
    let _static_registration_decorators;
    let _static_createToken_decorators;
    let _static_login_decorators;
    return _a = class User {
            static async getByToken({ prisma, token }) {
                return await prisma.user.findFirst({
                    where: { token },
                    select: {
                        email: true,
                        name: true,
                        password: true,
                        token: true,
                    },
                });
            }
            static async registration({ email, password, prisma }) {
                return await prisma.user
                    .create({
                    data: {
                        email,
                        password: (0, md5_1.default)(password),
                        token: (0, md5_1.default)(Date.now().toString() + password),
                    },
                })
                    .catch((error) => console.log(error));
            }
            static async createToken({ email, password, prisma }) {
                return await prisma.user
                    .update({
                    where: { email, password: (0, md5_1.default)(password) },
                    data: {
                        token: (0, md5_1.default)(Date.now().toString() + password),
                    },
                })
                    .catch((error) => console.log(error));
            }
            static async login({ email, password }) {
                const user = await _a.createToken({
                    email: email,
                    password: password,
                }).catch((error) => console.log(error));
                return user === null || user === void 0 ? void 0 : user.token;
            }
        },
        (() => {
            const _metadata = typeof Symbol === "function" && Symbol.metadata ? Object.create(null) : void 0;
            _static_getByToken_decorators = [DB_1.DB];
            _static_registration_decorators = [DB_1.DB];
            _static_createToken_decorators = [DB_1.DB];
            _static_login_decorators = [DB_1.DB];
            __esDecorate(_a, null, _static_getByToken_decorators, { kind: "method", name: "getByToken", static: true, private: false, access: { has: obj => "getByToken" in obj, get: obj => obj.getByToken }, metadata: _metadata }, null, _staticExtraInitializers);
            __esDecorate(_a, null, _static_registration_decorators, { kind: "method", name: "registration", static: true, private: false, access: { has: obj => "registration" in obj, get: obj => obj.registration }, metadata: _metadata }, null, _staticExtraInitializers);
            __esDecorate(_a, null, _static_createToken_decorators, { kind: "method", name: "createToken", static: true, private: false, access: { has: obj => "createToken" in obj, get: obj => obj.createToken }, metadata: _metadata }, null, _staticExtraInitializers);
            __esDecorate(_a, null, _static_login_decorators, { kind: "method", name: "login", static: true, private: false, access: { has: obj => "login" in obj, get: obj => obj.login }, metadata: _metadata }, null, _staticExtraInitializers);
            if (_metadata) Object.defineProperty(_a, Symbol.metadata, { enumerable: true, configurable: true, writable: true, value: _metadata });
            __runInitializers(_a, _staticExtraInitializers);
        })(),
        _a;
})();
exports.default = User;
