'use strict';
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Router_1 = __importDefault(require("./Router/Router"));
const Auth_1 = __importDefault(require("./controllers/Auth/Auth"));
const Booking_1 = __importDefault(require("./controllers/Booking/Booking"));
const Flight_1 = __importDefault(require("./controllers/Flight/Flight"));
class App {
    constructor() {
        this.env = {};
    }
    create() {
        new Router_1.default([Auth_1.default, Booking_1.default, Flight_1.default], {});
    }
}
const app = new App().create();
