import user from '../DBProvider/User';

export function Login(target: Function, _propertyKey: any): any {
  return async function (props: any): Promise<any> {
    const { response, request } = props;

    console.log(request?.headers);
    console.log(request?.body);

    if (!request.body?.email || !request.body?.password) {
      return response.json({
        error: 'Authentication error 1',
      });
    }

    const token = await user.login({
      email: request.body?.email,
      password: request.body?.password,
    });

    if (!token) {
      return response.json({
        error: 'Authentication error 2',
      });
    }

    const data = await target({ token });
    response.json(data);
  };
}
