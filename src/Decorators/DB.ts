import { PrismaClient } from '@prisma/client';

export function DB(target: Function, _propertyKey: any): any {
  return async function (props: any): Promise<any> {
    const { response, request, token } = props;

    const prisma = new PrismaClient();
    const result = await target({ ...props, prisma });

    await prisma.$disconnect();

    return result;
  };
}
