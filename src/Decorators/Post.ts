import user from '../DBProvider/User';

export function Post(target: Function, _propertyKey: any): any {
  return async function (props: any): Promise<any> {
    const { response, request } = props;
    const token: string = request?.body?.token || request?.headers['token'];
    const userData = await user.getByToken({ token });
    const isAuthenticated = !!userData?.token;

    if (!isAuthenticated) {
      return response.json({
        error: 'Authentication error',
      });
    }

    const data = await target({
      ...props,
      token: userData.token,
      userData,
      body: { ...request.body },
    });

    response.json(data);
  };
}
