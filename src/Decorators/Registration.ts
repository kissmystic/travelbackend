import user from '../DBProvider/User';

export function Registration(target: Function, _propertyKey: any): any {
  return async function (props: any): Promise<any> {
    const { response, request } = props;

    console.log(request?.headers);
    console.log(request?.body);

    if (!request.body?.email || !request.body?.password) {
      return response.json({
        error: 'Regsitration error',
      });
    }

    const userData = await user.registration({
      email: request.body?.email,
      password: request.body?.password,
    });

    if (!userData?.token) {
      return response.json({
        error: 'Authentication error',
      });
    }

    const data = await target({ token: userData?.token });
    response.json(data);
  };
}
