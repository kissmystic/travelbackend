import { Post } from '../../Decorators/Post';

import locaions from '../../data/locations.json';

export default class Booking {
  @Post
  static getLocationListByFilterString({ body }: { body: any }) {
    const searchString = body.searchString;
    if (!searchString) {
      return {};
    }

    const toSearch = searchString.match(/[a-zA-Z0-9]+/g)?.[0];

    console.log(toSearch);

    return locaions.filter(({ name }: { name: string }) =>
      name?.toLowerCase()?.includes?.(searchString.toLowerCase())
    );
  }
}
