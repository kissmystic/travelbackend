import { Post } from '../../Decorators/Post';

import flights from '../../data/flights.json';

import { FlightSearchParams } from './types';

export default class Flight {
  @Post
  static search({ body }: { body: FlightSearchParams }) {
    const legs = body.legs;

    return flights;
  }
}
