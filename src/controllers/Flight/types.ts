export enum FareClassTypes {
  economy = 'economy',
  premiumEconomy = 'premium economy',
  business = 'business',
  first = 'first',
}

export type FlightSearchParams = {
  legs: [
    {
      departureDate: string;
      from: string;
      to: string;
    }
  ];
  benchmarks: {
    class: FareClassTypes;
  };
};
