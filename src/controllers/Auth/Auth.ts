import { Login } from '../../Decorators/Login';
import { Registration } from '../../Decorators/Registration';

export default class Auth {
  @Login
  static login({ token }: { token: string }) {
    return { token };
  }

  @Registration
  static registration({ token }: { token: string }) {
    return { token };
  }
}
