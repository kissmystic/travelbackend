'use strict';

import Router from './Router/Router';
import Auth from './controllers/Auth/Auth';
import Booking from './controllers/Booking/Booking';
import Flight from './controllers/Flight/Flight';

class App {
  env = {};
  constructor() {}
  create() {
    new Router([Auth, Booking, Flight], {});
  }
}

const app = new App().create();
