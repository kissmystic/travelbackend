import md5 from 'md5';

import { DB } from '../Decorators/DB';

export default class User {
  @DB
  static async getByToken({ prisma, token }: any) {
    return await prisma.user.findFirst({
      where: { token },
      select: {
        email: true,
        name: true,
        password: true,
        token: true,
      },
    });
  }

  @DB
  static async registration({ email, password, prisma }: any) {
    return await prisma.user
      .create({
        data: {
          email,
          password: md5(password),
          token: md5(Date.now().toString() + password),
        },
      })
      .catch((error: Error) => console.log(error));
  }

  @DB
  static async createToken({ email, password, prisma }: any) {
    return await prisma.user
      .update({
        where: { email, password: md5(password) },
        data: {
          token: md5(Date.now().toString() + password),
        },
      })
      .catch((error: Error) => console.log(error));
  }

  @DB
  static async login({ email, password }: any) {
    const user = await User.createToken({
      email: email,
      password: password,
    }).catch((error: Error) => console.log(error));

    return user?.token;
  }
}
