import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';

import { Route } from './types';

export default class Router {
  controllers: any[];
  list: Route[] = [];
  params: any[];

  constructor(controllers: any[], params: any) {
    this.controllers = controllers;
    this.params = params;
    this.buildList();
    this.create();
  }

  buildList() {
    this.controllers.forEach((controller) => {
      Object.getOwnPropertyNames(controller).forEach((route) => {
        if (route !== 'length' && route !== 'name' && route !== 'prototype') {
          this.list.push({
            name: `/${controller.name.toLowerCase()}/${route}`,
            method: route,
            controller: controller,
          });
        }
      });
    });
  }

  create() {
    const app = express();
    app.use(
      bodyParser.urlencoded({
        extended: false,
      })
    );
    app.use(bodyParser.json());
    app.use(cors());

    for (let i = 0; i <= this.list.length - 1; i++) {
      const route: Route = this.list[i];
      app.post(route.name, (request, response, next) =>
        route.controller[route.method]({
          request,
          response,
          next,
          ...this.params,
        })
      );
    }

    app.listen('8002', function () {
      console.log('server started');
    });
  }
}
