export type Route = {
  name: string;
  method: string;
  controller: any;
};
